import { CFG } from './config.mjs';
import { NoteEditor } from './editor.mjs';

function openNoteManager(event) {
	return new NoteEditor().render(true);
}

/**
 * @param {JournalDirectory} app
 * @param {JQuery} jq
 * @param {Object} options
 */
async function initJournalNotes(app, jq, _options) {
	console.log('NOTES |', game.user);

	const html = jq[0];
	const header = html.querySelector('.directory-header');

	async function findOrCreateFolder() {
		const folder = app.folders.find(f => f.type === 'JournalEntry' && f.getFlag(CFG.module, 'hide'));
		if (folder) return folder;
		return Folder.create({
			name: CFG.name,
			type: 'JournalEntry',
			parent: null,
			color: '#ff0000',
			flags: { 'mana-notes': { hide: true, managed: { } } }
		});
	}

	const folder = CFG.folder = await findOrCreateFolder();
	if (!folder) return;

	const el = html.querySelector(`[data-folder-id="${folder.id}"]`);
	if (!el) return;
	el.remove();

	console.log('NOTES | Folder Hidden:', folder.id, folder);

	const div = document.createElement('div');
	div.classList.add('flexrow', 'mana-notes');

	const button = document.createElement('button');
	button.classList.add('mana-manage-notes');
	const symbol = document.createElement('i');
	symbol.classList.add('fas', 'fa-tasks');
	button.append(symbol);
	button.append(' Manage Notes');
	div.append(button);

	header.append(div);

	button.addEventListener('click', openNoteManager);
}

Hooks.on('renderJournalDirectory', initJournalNotes);

Hooks.once('ready', () => openNoteManager());

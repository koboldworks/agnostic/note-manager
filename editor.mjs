import { CFG } from './config.mjs'

export class NoteEditor extends FormApplication {
	/** @type {JournalEntry} */
	document;
	/** @type {String} */
	documentId;

	get template() {
		return `modules/${CFG.module}/editor.hbs`;
	}

	getData() {
		const data = super.getData();
		const folder = CFG.folder;
		console.log('NOTE MANAGER | Note folder:', folder);
		data.journals = folder.documentCollection.filter(j => j.folder === folder);
		data.folder = folder;

		data.documentId = this.documentId;
		data.document = this.document;
		data.documentName = this.document?.name ?? 'n/a';
		if (this.document)
			data.path = this.document.getFlag(CFG.module, CFG.SETTINGS.pathFlag);

		data.documentContent = this.document?.data.content;

		return data;
	}

	static get defaultOptions() {
		const _default = super.defaultOptions;
		return mergeObject(_default, {
			id: 'mana-notes-manager',
			title: 'Note Manager',
			closeOnSubmit: false,
			submitOnClose: true,
			submitOnChange: true,
			top: 100,
			resizable: true,
			classes: [..._default.classes, 'mana-note-manager'],
			scrollY: ['.journal-list'],
		});
	}

	/**
	 * @param {Event} event
	 * @param {Object} formData
	 */
	_updateObject(event, formData) {
		console.log('_updateObject:', formData);

		if (this.document && formData.description) {
			this.document.update({ content: formData.description })
				.then(_ => {
					console.log('UpdateComplete:', this.document.id);
					this.render()
				});
		}
	}

	/**
	 * @param {Event} event
	 */
	async _createNote(event) {
		event.preventDefault();
		event.stopPropagation();

		// Query note name
		const entry = await JournalEntry.create({
			name: 'New managed note',
			folder: CFG.folder.id,
			flags: { 'mana-notes': { managed: { folder: null } } }
		});

		this.document = entry;
		this.documentId = entry.id;
	}

	/**
	 * @param {Event} event
	 */
	_titleChange(event) {
		event.preventDefault();
		event.stopPropagation();

		console.log(event);

		const editing = event.target.attributes.name.value;
		console.log('input focus lost:', event);
		ui.notifications.info(`Input Focus lost [${this.document.id}] on "${editing}"`);

		const el = event.target;
		const value = el.textContent?.trim();

		if (value.length > 0 && value !== this.document.name) {
			this.document.update({ name: value })
				.then(_ => {
					console.log('Document title changed:', value);
					this.render();
				});
		}
	}

	/**
	 * @param {Event} event
	 */
	_pathChange(ev) {
		event.preventDefault();
		event.stopPropagation();

		const el = event.target;
		const value = el.textContent?.trim();

		if (value.length > 0 && value !== this.document.getFlag(CFG.module, CFG.SETTINGS.pathFlag)) {
			console.log('Path name changed:', value);
			this.document.setFlag(CFG.module, CFG.SETTINGS.pathFlag, value)
				.then(_ => {
					console.log('Path changed to:', value);
				});
		}
	}

	_inputFocusGained(event) {
		console.log('input focus gained:', event);
		const editing = event.target.attributes.name.value;
		ui.notifications.info(`Input Focus gained [${this.document.id}] on "${editing}"`);
	}

	/**
	 * @override
	 */
	render(force, options = {}) {
		if (this.document) this.options.title = `Note Manager: ${this.document.name}`;

		if (this._editing) {
			if (force) this.__pendingRefresh = true;
			console.log('REFRESH LOCK | Editing, blocking refresh');
			return;
		}

		// Special case for tinyMCE editors
		const dirtyEditors = tinymce.editors.filter(e => !e.isNotDirty);
		if (dirtyEditors.length > 0) {
			for (const editor of dirtyEditors) {
				const el = this.form.querySelector(`#${editor.id}`);
				if (el) {
					this.__rl_editing = true;
					editor.on('blur', () => this.__rl_editing = false);
					console.log('NOTE MANAGER | tinyMCE dirty, blocking refresh');
					return;
				}
			}
		}

		super.render(force, options)
	}

	/**
	 * @param {Event} event
	 */
	_clickEntry(event) {
		const baseEl = event.target;
		const el = baseEl.matches('[data-journal-id]') ? baseEl : baseEl.closest('[data-journal-id]');
		if (!el) return;
		const jid = el.dataset.journalId;

		if (!jid) return;

		event.preventDefault();
		event.stopPropagation();

		if (jid === this.documentId) {
			console.warn('NOTE MANAGER | Attempting to re-open same journal:', jid);
			return;
		}

		const journal = game.journal.get(jid);

		console.log('NOTE MANAGER | Open note:', jid, journal);

		if (!journal) {
			console.error('NOTE MANAGER | Journal is missing:', jid);
			return;
		}

		this.documentId = jid;
		this.document = journal;
		this.render();
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);
		const html = jq[0];

		// New note button
		const button = html.querySelector('button[data-action="create-note"]');
		button?.addEventListener('click', this._createNote.bind(this));

		// Clicking existing notes
		html.querySelector('.journal-list')
			?.addEventListener('click', this._clickEntry.bind(this));

		// Journal title editing
		const titleEditor = html.querySelector('[name="name"]');
		titleEditor.addEventListener('blur', this._titleChange.bind(this));
		const pathEditor = html.querySelector('[name="path"]');
		pathEditor?.addEventListener('blur', this._pathChange.bind(this));
		html.querySelectorAll('[contenteditable]').forEach(el => {
			el.addEventListener('focus', this._inputFocusGained.bind(this));
			el.addEventListener('keydown', function (event) {
				if (event.keyCode == 13) {
					// Prevent enter key doing its weird <div><br></div> and instead blur the input
					event.preventDefault();
					event.target.blur();
					return false;
				}
			});
		});

		// Refresh locking

		jq.on('focus blur', 'input,textarea,[contenteditable]', function (event) {
			// jquery uses focusin/focusout
			const editing = event.target.attributes.name.value;
			if (event.type === 'focusin') {
				console.log('REFRESH LOCK | Edit start:', editing);
				this.__rl_editing = true;
				this.__rl_editingField = editing;
			}
			else { // 'focusout'
				console.log('REFRESH LOCK | Edit stop:', this.__rl_editingField);
				this.__rl_editing = false;
				this.__rl_editingField = undefined;
			}
		});
	}
}

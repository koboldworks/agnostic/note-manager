# Note Manager

WIP

Shared note management with simultaneous edit protection.

## Install

Manifest URL: <https://gitlab.com/koboldworks/agnostic/note-manager/-/raw/latest/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT license](./LICENSE).
